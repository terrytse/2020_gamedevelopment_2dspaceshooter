﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PICKUP_TYPE { MANNA, IMMUNITY , LIFE};
public class Pickup : MonoBehaviour
{
    [Header("General parameters")]
    public PICKUP_TYPE pickupType;
    public bool isCollectUsingMissile = false;
    public GameObject pickupEffect;

    [Header("Immunity Puckup parameters")]
    public float immunityTime = 6; //in seconds

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") || 
            (isCollectUsingMissile && collision.gameObject.CompareTag("Missile")))
        {
            if (pickupType == PICKUP_TYPE.MANNA)
                OnMannaPickup();
            else if (pickupType == PICKUP_TYPE.IMMUNITY)
                OnImmunityPickup();
            else if (pickupType == PICKUP_TYPE.LIFE)
                OnLifePickup();
            else
            {
                Debug.LogError("Pickup not programmed for this type: " + pickupType);
            }
            //also instantiate an effect
            //you should do it for your term project
            Destroy(gameObject);
        }      
    }

    void OnMannaPickup()
    {
        Debug.Log("Collected a healtl pickup");
        GameObject.FindObjectOfType<PlayerController>().ResetHealthToMax();
    }

    void OnImmunityPickup()
    {
        GameObject.FindObjectOfType<PlayerController>().TurnOnImmunityMode(immunityTime);
    }

    void OnLifePickup()
    {
        GameObject.FindObjectOfType<GameManager>().AddLife();
    }
}
