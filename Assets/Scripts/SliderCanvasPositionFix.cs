﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Apply this script on the canvas of the slider (not directly on the slider).
public class SliderCanvasPositionFix : MonoBehaviour
{
    private Vector3 offset;
    private Collider2D parentCollider;


    // Use this for initialization
    void Awake()
    {
        parentCollider = transform.parent.GetComponent<PolygonCollider2D>();
        offset = parentCollider.bounds.center - transform.position;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = parentCollider.bounds.center - offset;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, parentCollider.transform.rotation.z * -1.0f);
    }
}
