﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Slider playerHealthSlider;


    // change in the inspector depensing upon the movement if it is too slow or too fast
    public float movementSpeed = 8f;


    // where the bullets are going to be spawned
    public Transform missileSpawnPoint;


    // this is our prefab that will spawn when we press 'Fire1' key (left ctrl)
    public GameObject missilePrefab;

    public GameObject shield;

    private bool isImmunityModeOn = false;

    public float missileForce = 10f;

    private GameManager gameMgr;

    private void Awake()
    {
        playerHealthSlider.value = 100;
        gameMgr = GameObject.FindObjectOfType<GameManager>();
    }

    public void TurnOnImmunityMode(float expiryTime)
    {
        isImmunityModeOn = true;
        Invoke("TurnOffImmunityMode", expiryTime);
        shield.SetActive(true);
        gameMgr.immunitySlider.transform.parent.gameObject.SetActive(true);

        gameMgr.immunitySlider.maxValue = expiryTime;
        gameMgr.immunitySlider.value = expiryTime;
        InvokeRepeating("UpdateImmunitySlider", 0, 1.0f);
    }

    void UpdateImmunitySlider()
    {
        gameMgr.immunitySlider.value -= 1.0f;
    }

    private void TurnOffImmunityMode()
    {
        isImmunityModeOn = false;
        CancelInvoke("UpdateImmunitySlider");
        gameMgr.immunitySlider.transform.parent.gameObject.SetActive(false);
        shield.SetActive(false);
    }

    public void onPlayerHit(int damage)
    {
        if (isImmunityModeOn) return;

        playerHealthSlider.value -= damage;

        if(playerHealthSlider.value <= 0)
        {
            //does player have more than one life?
            //then reduce life, and reset health to max
            //otherwise, trigger game over.
            GameObject.FindObjectOfType<GameManager>().onPlayerKilled();
        }
    }

    public void ResetHealthToMax()
    {
        playerHealthSlider.value = 100;
    }

    // Update is called once per frame
    void Update()
    {
        float translationX = Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime;

        //move the spaceship in the X axis only
        transform.Translate(translationX, 0, 0);

        if(Input.GetKeyUp(KeyCode.Space))
        {
            GameObject go = Instantiate(missilePrefab, missileSpawnPoint);

            //we use Rigidbody2D instead of Rigidbody in 3D
            //add force so that the missile can travel forward. propel missile in the forward direction of the spaceship.
            go.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, missileForce));
            Destroy(go, 1.5f); //destroy the missile after 3 seconds (if it doesnt already get destoryed upon hitting a collider)
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == ("SpaceshipParts"))
        {
            Debug.Log("ShipDestroyed");
        }
        else if (collision.gameObject.tag == ("Asteroid"))
        {
            Debug.Log("ShipDestroyed Asteroid");
        }
    }

    public void DamageFromPlanet(float planetDamage)
    {
        playerHealthSlider.value -= planetDamage;
    }
}
