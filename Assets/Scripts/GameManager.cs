﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int totalLives = 3;
    /// <summary>
    /// the number of levels in the game
    /// </summary>
    public int totalLevels = 3;
    /// <summary>
    /// shows when the level is successfully completed
    /// </summary>
    public GameObject levelCompletedScreen;
    public GameObject gameOverScreen;
    /// <summary>
    /// score will be increased when you hit an enemy or destroy certain objects...
    /// hitting enemy ships will increase score based on how powerful the enemy ships are..
    /// //hitting asteroids will also increase score...
    /// </summary>
    public int score;
    public int highScore;
    /// <summary>
    /// these are the total asteroids in the scene. once you hit all of them, your game is complete...
    /// </summary>
    public int totalEnemiesToDestroy;
    /// <summary>
    /// increments by 1 upon hitting each asteroid...
    /// </summary>
    public int totalEnemiesHit;
    //we keep track of how many asteroids were missed...
    public int totalEnemiesMissed;
    [SerializeField]
    private int currentLevelNumber = 1;

    public Slider immunitySlider;

    public GameObject highScoreCanvas;
    public GameObject howToPlayCanvas;
    public GameObject HUD;

    public Text livesTextField;
    public Text scoreTextField;
    public Text menuHighTextField;
    public Text finalScoreTextField;
    public Text highScoreTextField;
    public Text skillLevelTextField;

    private void Awake()
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
    }
    void LoadLevel()
    {
        string fileName = "MainLevel" + currentLevelNumber;
        SceneManager.LoadScene(fileName, LoadSceneMode.Additive);
    }
    public void RegisterEnemy()
    {
        totalEnemiesToDestroy++;
    }

    public void AddLife()
    {
        totalLives++;
        livesTextField.text = "Lives: " + totalLives;
    }

    public void OnAsteroidHit()
    {
        totalEnemiesHit++;
        score += 5; // each asteroid destroyed earns 5 points...
        scoreTextField.text = "Score: " + score;
        CheckGameOver();
    }

    public void OnSpaceshipPartsHit()
    {
        score += 2; // each spaceship part destroyed earns 2 points...
        scoreTextField.text = "Score: " + score;
        CheckGameOver();
    }

    public void OnAsteroidMissed()
    {
        totalEnemiesMissed++;
        CheckGameOver();
    }
    void CheckGameOver()
    {
        //if we have destroyed all asteroids available in the level, 
        //then game is over..
        if ((totalEnemiesHit + totalEnemiesMissed) == totalEnemiesToDestroy)
        {
            if (currentLevelNumber < totalLevels)
            {
                levelCompletedScreen.SetActive(true);
                PauseGame();
            }
            else
            {
                PauseGame();
                gameOverScreen.SetActive(true);
                DisplayEndScreenInfo();
            }           
        }
    }

    public void DisplayEndScreenInfo()
    {
        if(score > 360)
            skillLevelTextField.text = "Skill Level: Expert!";
        else if(score > 350)
            skillLevelTextField.text = "Skill Level: Pro!";
        else if (score > 340)
            skillLevelTextField.text = "Skill Level: Average!";
        else
            skillLevelTextField.text = "Skill Level: Noooooob!";

        if(score > PlayerPrefs.GetInt("Highscore", 0))
        {
            PlayerPrefs.SetInt("HighScore", score);
            finalScoreTextField.text = "Your Score: " + score;
            highScoreTextField.text = "You have beaten the previous high score. New: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        }
        else
        {
            finalScoreTextField.text = "Your Score: " + score;
            highScoreTextField.text = "HighScore: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        }  
    }

    public void ShowHighScore()
    {
        highScoreCanvas.SetActive(true);
        menuHighTextField.text = "All Time High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void ReloadCurrentLevel()
    {
        string fileName = "MainLevel" + currentLevelNumber;
        SceneManager.UnloadSceneAsync(fileName);
        LoadLevel();
    }
    public void LoadNextLevel()
    {
        string fileName = "MainLevel" + currentLevelNumber;
        SceneManager.UnloadSceneAsync(fileName);
        currentLevelNumber++;
        LoadLevel();
        UnpauseGame();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
    }

    public void StartLevel1()
    {     
        totalEnemiesToDestroy = 0;
        totalEnemiesHit = 0;
        totalEnemiesMissed = 0;
        LoadLevel();
        SceneManager.UnloadSceneAsync(1);
        HUD.SetActive(true);
        livesTextField.text = "Lives: " + totalLives;
        scoreTextField.text = "Score: " + score;
        UnpauseGame();
    }       

    public void BackToMenu()
    {
        UnpauseGame();
        SceneManager.UnloadSceneAsync(4);
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        totalEnemiesToDestroy = 0;
        totalEnemiesHit = 0;
        totalEnemiesMissed = 0;
        currentLevelNumber = 1;
    }

    public void LoadMenuScreen()
    {
        SceneManager.UnloadSceneAsync(1);
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
    }

    public void Replay()
    {
        totalEnemiesToDestroy = 0;
        totalEnemiesHit = 0;
        totalEnemiesMissed = 0;
        currentLevelNumber = 1;
        livesTextField.text = "Lives: " + totalLives;
        scoreTextField.text = "Score: " + score;
        UnpauseGame();
        SceneManager.UnloadSceneAsync(4);
        LoadLevel();
    }

    public void onPlayerKilled()
    {
        if(totalLives > 0)
        {
            totalLives--;
            livesTextField.text = "Lives " + totalLives;

            if(totalLives > 0)
            {
                GameObject.FindObjectOfType<PlayerController>().ResetHealthToMax();              
            }
            else
            {
                Debug.Log("Game Over as you lost all lives");
                PauseGame();
                gameOverScreen.SetActive(true);
                DisplayEndScreenInfo();
            }
        }
    }
}