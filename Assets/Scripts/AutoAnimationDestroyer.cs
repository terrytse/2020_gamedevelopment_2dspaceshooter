﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAnimationDestroyer : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        float timeToDestroy = GetComponent<ParticleSystem>().main.duration;
        Destroy(gameObject, timeToDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
