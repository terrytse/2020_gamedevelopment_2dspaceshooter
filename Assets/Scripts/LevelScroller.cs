﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScroller : MonoBehaviour
{
    public float scrollSpeed = -500;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float translationY = scrollSpeed * Time.deltaTime;
        transform.Translate(0, translationY, 0);
    }
}
