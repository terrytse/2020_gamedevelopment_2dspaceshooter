﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAsteroid : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Asteroid"))
        {
            Destroy(collision.gameObject);
            GameObject.FindObjectOfType<GameManager>().OnAsteroidMissed();

            //instantiate particles...
            //Asteroid get destroyed...
        }
        else if (collision.CompareTag("SpaceshipParts"))
        {
            Destroy(collision.gameObject);
            //GameObject.FindObjectOfType<GameManager>().OnAsteroidMissed();
        }
    }  
}
