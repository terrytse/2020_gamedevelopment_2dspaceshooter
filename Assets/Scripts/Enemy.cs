﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ENEMY_TYPE {ONE_SHOT, HEALTH_TYPE , ENEMYSHIP};
public class Enemy : MonoBehaviour
{
    public ENEMY_TYPE enemyType;
    public Slider healthSlider;

    public Transform missileSpawnPoint;
    public Missile missilePrefab;

    //delay between each fire
    public float fireRateDelay = 1;

    public float shipOffsetRotation = 90.0f;
    Transform playerShipTarget;

    private bool isInView = false;

    private void Awake()
    {
        if (!gameObject.CompareTag("SpaceshipParts"))
            GameObject.FindObjectOfType<GameManager>().RegisterEnemy();

        if (enemyType == ENEMY_TYPE.ONE_SHOT)
        {
            if (healthSlider != null)
                healthSlider.gameObject.SetActive(false);
        }
        else
            healthSlider.value = 100;

        playerShipTarget = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
        if (enemyType == ENEMY_TYPE.ENEMYSHIP)
        {
            LookAtTarget(playerShipTarget);        
        }
    }

    void Fire()
    {
        Missile go = Instantiate(missilePrefab, missileSpawnPoint);

        //we use Rigidbody2D instead of Rigidbody in 3D
        //add force so that the missile can travel forward. propel missile in the forward direction of the spaceship.
        go.GetComponent<Rigidbody2D>().AddForce(transform.up * go.missileForce);
    }

    void LookAtTarget(Transform target)
    {
        Vector3 diff = target.position - transform.position;
        diff.Normalize();

        float zRotation = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, zRotation - shipOffsetRotation);
    }

    private void LateUpdate()
    {
        if (healthSlider != null)
            healthSlider.transform.parent.rotation = Quaternion.identity;
    }

    public bool isEnemyKilled(int damage)
    {
        if(enemyType == ENEMY_TYPE.HEALTH_TYPE || enemyType == ENEMY_TYPE.ENEMYSHIP)
        {
            healthSlider.value -= damage;

            if (healthSlider.value <= 0)
            {
                return true;
            }
            else
                return false;
        }
        else if (enemyType == ENEMY_TYPE.ONE_SHOT)
        {
            return true;
        }
        else
        {
            Debug.LogError("The script is not programmed for this enemyType: " + enemyType);
            return false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("TopTrigger"))
        {
            isInView = true;

            if (enemyType == ENEMY_TYPE.ENEMYSHIP)
                InvokeRepeating("Fire", fireRateDelay, fireRateDelay);
        }
    }
}