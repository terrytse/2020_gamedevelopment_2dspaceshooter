﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMissiles : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Missile"))
        {
            Destroy(collision.gameObject);
        }
    }
}
