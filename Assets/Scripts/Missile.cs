﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MISSILE_TYPE { PLAYER_MISSILE, ENEMY_MISSILE };
public class Missile : MonoBehaviour
{
    public MISSILE_TYPE missileType;
    public float missileForce = 10000;
    public int damage = 10;

    public GameObject explosionParticle;
    public GameObject SpaceShipPartsParticles;
    public GameObject AsteroidParticles;

    private GameManager gmgr;

    void Start()
    {
        gmgr = GameObject.FindObjectOfType<GameManager>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (missileType == MISSILE_TYPE.PLAYER_MISSILE && collision.gameObject.tag == "Asteroid")// should be enemy
        {
            DestroyMissileAndPlayExplosionEffect();

            Enemy enemyRef = collision.gameObject.GetComponent<Enemy>();

            if (enemyRef.isEnemyKilled(damage))
            {
                GameObject asteroidExplosion = Instantiate(AsteroidParticles, transform.position, Quaternion.identity);
                Destroy(collision.gameObject);
                gmgr.OnAsteroidHit();
            }
        }
        else if (collision.gameObject.tag == "SpaceshipParts")
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject SSPartsExplosion = Instantiate(SpaceShipPartsParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            gmgr.OnSpaceshipPartsHit();
        }
        else if (collision.gameObject.tag == "DecoyAsteroid")
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject SSPartsExplosion = Instantiate(SpaceShipPartsParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "DecoySpaceshipParts")
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject SSPartsExplosion = Instantiate(SpaceShipPartsParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "JumpDownAsteroid") //jump back down to the bottom of the menu screen.
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject asteroidExplosion = Instantiate(AsteroidParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            gmgr.LoadMenuScreen();
        }
        else if (collision.gameObject.tag == "StartAsteroid") //start game.
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject asteroidExplosion = Instantiate(AsteroidParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            gmgr.StartLevel1();
        }
        else if (collision.gameObject.tag == "HighScoreAsteroid")
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject asteroidExplosion = Instantiate(AsteroidParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            gmgr.ShowHighScore();
        }
        else if (collision.gameObject.tag == "HowToPlayAsteroid") //open how to play canvas.
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            GameObject asteroidExplosion = Instantiate(AsteroidParticles, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            gmgr.PauseGame();
            gmgr.howToPlayCanvas.SetActive(true);
        }
        else if (collision.gameObject.tag == "QuitGameAsteroid") //to quit game.
        {
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);

            Destroy(collision.gameObject);
            Destroy(gameObject);

            Application.Quit();
        }
        else if (missileType == MISSILE_TYPE.ENEMY_MISSILE && collision.gameObject.tag == "Player")
        {
            DestroyMissileAndPlayExplosionEffect();

            PlayerController playerRef = collision.gameObject.GetComponent<PlayerController>();

            playerRef.onPlayerHit(damage);
        }
    }

    void DestroyMissileAndPlayExplosionEffect()
    {
        Destroy(gameObject);
        GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
    }
}
