﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicPlanet : MonoBehaviour
{
    public float damageRate;

    public float planetDamage = 2;

    float nextDamage;

    public void Start()
    {
        nextDamage = Time.time;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && nextDamage < Time.time)
        {
            GameObject.FindObjectOfType<PlayerController>().DamageFromPlanet(planetDamage);
            nextDamage = Time.time + damageRate;
        }
    }
}
